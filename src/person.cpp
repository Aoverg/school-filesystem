#include "person.h"
#include <string>

Person::Person(){};

Person::Person(std::string firstName, std::string lastName)
	{
		this->firstName = firstName;
		this->lastName = lastName;
	}

std::string Person::get_fullName()
{
	return firstName + " " + lastName;
}