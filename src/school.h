#pragma once
#include <fstream>
#include <vector>
#include <map>
#include "person.h"
#include "teacher.h"
#include "student.h"
#include "course.h"

class School
{
protected:
	//std::map<int, Person> people;
	std::map<int, Teacher> teachers;
	std::map<int, Student> students;
	std::map<int, Course> courses;

public:
	School(){};

	void writeSchool();

	//void add_person(int id, Person &person);
	void add_student(int id, Student student);
	void add_teacher(int id, Teacher teacher);
	void add_course(int id, Course course);

	Teacher get_teacher(int id);
	
	int get_lastID(int choice);

	template <class Serializer>
	void serialize(Serializer &s) const
	{
		s << teachers << students << courses;
	}

	template <class Serializer>
	void parse(Serializer &s)
	{
		s >> teachers >> students >> courses;
	}
};