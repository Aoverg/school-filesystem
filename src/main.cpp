#include <fstream>
#include <iostream>
#include <vector>
#include <string>

#include "../hps/hps.h"
#include "person.h"
#include "school.h"
#include "teacher.h"
#include "course.h"

void loadOldFile(School &file)
{
	if (auto dataFile = std::ifstream("src/test.txt", std::ios::binary))
	{
		file = hps::from_stream<School>(dataFile);
		dataFile.close();
	}
	else
		std::cout << "Error: Could not open file";
}

// print to file
void writer(School &file)
{
	if (auto dataFile = std::ofstream("src/test.txt", std::ios::binary))
	{
		hps::to_stream(file, dataFile);
		dataFile.close();
	}
	else
		std::cout << "Error: Could not write to file";
}

// to test while programming
void reader(School &file)
{
	std::ifstream dataFileIn("src/test.txt", std::ios::binary);
	file = hps::from_stream<School>(dataFileIn);
	dataFileIn.close();
}

int amount;

int main()
{
	School school;
	loadOldFile(school);

	// start
	int promt1 = 0;
	bool running = true;

	while (running)
	{
		std::cout << "(1) Find person\n(2) add school member\n(3) Courses\n (4) Quit\n: ";
		std::cin >> promt1;

		switch (promt1)
		{
		case 1: // find person
			int promt1_1;
			std::cout << "(1) teachers\n(2) students";
			std::cin >> promt1_1;

			switch (promt1_1)
			{
			case 1:
				for (int i = 0; i < school.get_lastID(1); i++)
				{
					std::cout << school.get_teacher(i).get_fullName();
				}
					break;

			default:
				break;
			}
			break;
		default:
			break;
		}
	}

	return 0;
}