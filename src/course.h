#pragma once

#include <vector>

class Course
{
protected:
	std::string subject;
	std::vector<std::string> students;
	std::vector<std::string> teachers;

public:
	Course();

	Course(std::string subject, std::vector<std::string> students, std::vector<std::string> teachers);

template <class Serializer>
void serialize(Serializer &s) const
{
	s << subject << students << teachers;
}

template <class Serializer>
void parse(Serializer &s)
{
	s >> subject >> students >> teachers;
}

};