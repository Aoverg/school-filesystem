#pragma once
#include <fstream>
#include <string>

class Person
{
protected:
	std::string firstName;
	std::string lastName;

public:
	Person();
	Person(std::string firstName, std::string lastName);
	std::string get_fullName();

	template <class Serializer>
	void serialize(Serializer &s) const
	{
		s << firstName << lastName;
	}

	template <class Serializer>
	void parse(Serializer &s)
	{
		s >> firstName >> lastName;
	}
};
