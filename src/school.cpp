#include "school.h"

// void School::add_person(int id, Person &person)
// {
// 	people[id] = person;
// }

void School::add_student(int id, Student student)
{
	students[id] = student;
}

void School::add_teacher(int id, Teacher teacher)
{
	teachers[id] = teacher;
}

void School::add_course(int id, Course course)
{
	courses[id] = course;
}

int School::get_lastID(int choice)
{
	switch (choice)
	{
	case 0:
		return students.size();
		break;
	case 1:
		return teachers.size();
		break;
	case 3:
		return courses.size();	
	default:
		break;
	}
	return 0;
}

Teacher School::get_teacher(int id)
{
	return teachers[id];
}