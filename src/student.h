#pragma once
#include "person.h"
#include <vector>

class Student : public Person
{
protected:
	std::vector<std::string> courses;

public:
	Student();
	Student(std::string firstName, std::string lastName);

	template <class Serializer>
	void serialize(Serializer &s) const
	{
		Person::serialize(s);
		s << courses;
	}

	template <class Serializer>
	void parse(Serializer &s)
	{
		Person::parse(s);
		s >> courses;
	}
};