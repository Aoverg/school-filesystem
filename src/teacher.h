#pragma once
#include <fstream>
#include <string>
#include <vector>
#include "person.h"

class Teacher : public Person
{
	protected:
		std::vector<std::string> coursesTeaching;
	
	public:
		Teacher():Person(){};
		Teacher(std::string firstName, std::string lastName);

	template <class Serializer>
	void serialize(Serializer &s) const
	{
		Person::serialize(s);
		s << coursesTeaching;
	}

	template <class Serializer>
	void parse(Serializer &s)
	{
		Person::parse(s);
		s >> coursesTeaching;
	}
};